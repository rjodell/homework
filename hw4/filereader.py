# This function script will read all the lines of a  file and produce a histogram of
# all the letter counts in the file

# code by Ryan O'Dell
# UCSC AMS 129 Spring Quarter 2018

# IMPORTANT NOTE TO RUN THIS CODE

# this creates a module called filereader
# to run the code  run the following commands
# first make sure this file and words.txt are in the same directory
# then run 
# from filereader import histogram_producer
# histogram_producer()


import numpy as np

import matplotlib 
matplotlib.use('Agg')

from collections import OrderedDict
import matplotlib.pyplot as plt
# here we import these modules in a global fashion within our module
# if we were to add functions they into this we would be able to use 
# numpy and matplotlib in all of them 
# as opposed to importing them locally within each defined function 

def histogram_producer():
   # for the purpose of the assignment I didn't make it a proper function
   # the code could be easily modified to  a  more general purpose implementation
   # so that it could run on any file you want
   letters="abcdefghijklmnopqrstuvwxyz"
   # defines the letters we want our dictionary to store
   # we will have to make our .txt strictly lower case letters
  
   d=dict({'a':0,'b':0,'c':0,'d':0,'e':0,'f':0,'g':0,'h':0,'i':0,'j':0,'k':0,'l':0,'m':0,'n':0,'o':0,'p':0,'q':0,'r':0,'s':0,'t':0,'u':0,'v':0,'w':0,'x':0,'y':0,'z':0})
   # initializes the dictionary with zero values and only lower case letters
   file_path = './words.txt'
   # sets the file path to words.txt !!! must be in the same directory as this file ###
   with open(file_path, 'r') as f:
      value=f.read()
   value=value.lower()
   # reads the words.txt file and forces its to be all lower case

   for stuff in value:
      if stuff in letters:
         d[stuff] += 1
   # this loop iterates through  our string of letters and every time a letter occurs
   # in words.txt  we add one to count the number of letters in the file
   # print(d.values())
   lister=OrderedDict(sorted(d.items()))
   
  # lister=dict(lister)
   
   # had to  resort my dictionary since after adding all the values my dictionary became
   # unsorted alphabetically for some reason I don't exactly understand
   # sorted command makes the dictionary into a list for some odd reason
   # had to force the type back 
  
  # my dictionary is defined exactly as the assignment wants but thats easy to edit 

   x= np.arange(len(lister.keys()))
   y= np.zeros(len(lister.keys()))
   count = 0
   for i in letters:
      
       y[count]=lister[i]
       count +=1
   plt.interactive(False)
   plt.figure(figsize=(8,6) , dpi=80)
   plt.bar(x,y)
   plt.plot(x , y, 'ro-')
   plt.xticks(np.arange(26) , ('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z')  )
   plt.grid(True)
   plt.xlabel('Character')
   plt.ylabel('Frequency')
   plt.title('Alphabetically Arranged Letter Frequency')
   plt.savefig('results')
   return(lister)

   
